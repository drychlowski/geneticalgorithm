﻿using System;

namespace GeneticAlgorithm
{
    class Program
    {
        static void Main()
        {
            var population = Population.Create()
                .PopulationSize(8)
                //.FitnessFunction(new Polynomial(2, 0, 0))
                .FitnessFunction(new Polynomial(2, 5))
                .Range(0, 1)
                .GeneratePopulation();


            population.Resolve(0.8, 0.1, 50);
            Console.ReadKey();
        }
    }
}
