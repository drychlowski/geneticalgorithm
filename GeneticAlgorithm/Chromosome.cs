﻿using System;

namespace GeneticAlgorithm
{
    class Chromosome
    {
        public string Gens { get; set; }

        public Chromosome()
        {
            Gens = "0";
        }

        public Chromosome(string gens)
        {
            Gens = string.Copy(gens);
        }

        public Chromosome(Chromosome chromosome)
        {
            Gens = string.Copy(chromosome.Gens);
        }

        public int GetPhenotype()
        {
            return Convert.ToInt32(Gens, 2);
        }

        public double GetFitness(IFunction function)
        {
            return function.Value(GetPhenotype());
        }

        public void Mutate(int locus)
        {
            var gens = Gens.ToCharArray();
            gens[locus] = gens[locus] == '0' ? '1' : '0';
            Gens = new string(gens);
        }

        public bool InRange(int[] range)
        {
            return GetPhenotype() >= range[0] && GetPhenotype() <= range[1];
        }

        public void CrossWith(Chromosome otherChromosome, int locus)
        {
            if (Gens.Length != otherChromosome.Gens.Length)
                throw new ArgumentException("Chromosomes with different lenght");

            var gens = Gens.ToCharArray();
            var otherGens = otherChromosome.Gens.ToCharArray();

            for (var j = locus + 1; j < Gens.Length; j++)
            {
                gens[j] = otherGens[j];
                otherGens[j] = Gens[j];
            }

            Gens = new string(gens);
            otherChromosome.Gens = new string(otherGens);
        }
    }
}
