﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.PopulationBuiliderFluentApi;

namespace GeneticAlgorithm
{
    class Population
    {
        public IEnumerable<Chromosome> Chromosomes { get; set; }
        public IFunction FitnessFunction { get; set; }
        public int PopulationSize { get; set; }
        public int ChromosomeLenght { get; set; }
        public int[] Range { get; set; }
        private static readonly Random Random = new Random();

        private Population()
        {
        }

        public static IPopSize Create()
        {
            return new PopulationBuilider(new Population());
        }

        private double GetPopulationFitness()
        {
            return Chromosomes.Sum(chromosome => chromosome.GetFitness(FitnessFunction));
        }

        private Chromosome GetFittestChrmosome()
        {
            return Chromosomes.Aggregate((ch1, ch2) => ch1.GetFitness(FitnessFunction) > ch2.GetFitness(FitnessFunction) ? ch1 : ch2);
        }

        private void DoSelection()
        {
            var selectedChromosomes = new List<Chromosome>();
            var cumulatedSum = 0.0;
            var cumulatedValues = Chromosomes.Select(chromosome => cumulatedSum += chromosome.GetFitness(FitnessFunction)).ToList();

            for (var i = 0; i < PopulationSize; i++)
            {
                var drawnValue = Random.Next(0, (int)cumulatedValues.Last());
                var j = 0;

                foreach (var chromosome in Chromosomes)
                {
                    if (!(drawnValue < cumulatedValues[j++])) continue;
                    selectedChromosomes.Add(new Chromosome(chromosome));
                    break;
                }
            }

            Chromosomes = selectedChromosomes;
        }

        private void DoCrossover(double probability)
        {
            var chromosomes = Chromosomes.ToList();

            for (var i = 0; i < PopulationSize; i += 2)
            {
                if (Random.NextDouble() > probability) continue;
                var locus = Random.Next(0, ChromosomeLenght - 1);
                chromosomes[i].CrossWith(chromosomes[i + 1], locus);

                if (!chromosomes[i].InRange(Range)) ResolveNotInRange(chromosomes[i]);
                if (!chromosomes[i + 1].InRange(Range)) ResolveNotInRange(chromosomes[i + 1]);
            }

            Chromosomes = chromosomes;
        }

        private void DoMutation(double probability)
        {
            foreach (var chromosome in Chromosomes)
            {
                if (Random.NextDouble() > probability) continue;
                var locus = Random.Next(0, ChromosomeLenght);
                chromosome.Mutate(locus);

                if (!chromosome.InRange(Range)) ResolveNotInRange(chromosome);
            }
        }

        private void ResolveNotInRange(Chromosome chromosome)
        {
            while (!chromosome.InRange(Range))
            {
                var locus = Random.Next(0, ChromosomeLenght);
                chromosome.Mutate(locus);
            }
        }

        public void AddPopulationToPrint(int generation)
        {
            QueuedConsole.AddToPrint($"{generation} generation");

            foreach (var chromosome in Chromosomes)
            {
                QueuedConsole.AddToPrint(string.Join(",", chromosome.Gens.ToArray()) + $" Fitness: {chromosome.GetFitness(FitnessFunction)}");
            }

            QueuedConsole.AddToPrint($"Population fitness: {GetPopulationFitness()}");
            var fittestChromosome = GetFittestChrmosome();
            QueuedConsole.AddToPrint($"Fittest chromosome: {string.Join(",", fittestChromosome.Gens.ToArray())} Fitness: {fittestChromosome.GetFitness(FitnessFunction)} Phenotype: {fittestChromosome.GetPhenotype()}\n");
        }

        public void Resolve(double crossoverProbability, double mutationProbability, int iterationsForBestChromosome)
        {
            var repeats = 0;
            var generation = 0;
            AddPopulationToPrint(generation++);

            while (repeats < iterationsForBestChromosome)
            {
                var prevFittestChromosomeValue = GetFittestChrmosome().GetFitness(FitnessFunction);

                DoSelection();
                DoCrossover(crossoverProbability);
                DoMutation(mutationProbability);

                repeats++;
                if (prevFittestChromosomeValue > GetFittestChrmosome().GetFitness(FitnessFunction))
                {
                    repeats = 0;
                } 
                
                AddPopulationToPrint(generation++); 
            }

            QueuedConsole.PrintAll();
        }
    }
}
