﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticAlgorithm.PopulationBuiliderFluentApi;

namespace GeneticAlgorithm
{
    class PopulationBuilider : IPopSize, IAdaptationFunc, IRange, IGenPop
    {
        private readonly Population _beingConstructed;

        public PopulationBuilider(Population beingConstructed)
        {
            _beingConstructed = beingConstructed;
        }

        public IAdaptationFunc PopulationSize(int popSize)
        {
            if (popSize % 2 != 0) throw new ArgumentException("Population size must be divisible by 2");
            _beingConstructed.PopulationSize = popSize;
            return this;
        }

        public IRange FitnessFunction(IFunction function)
        {
            _beingConstructed.FitnessFunction = function;
            return this;
        }

        public IGenPop Range(int rangeFrom, int rangeTo)
        {
            _beingConstructed.Range = new [] { rangeFrom, rangeTo };
            return this;
        }

        public Population GeneratePopulation()
        {
            var chrmosomes = new List<Chromosome>();
            _beingConstructed.ChromosomeLenght = Convert.ToString(_beingConstructed.Range[1], 2).Length;
            var rand = new Random();

            for (var i = 0; i < _beingConstructed.PopulationSize; i++)
            {
                var gens = Convert.ToString(rand.Next(_beingConstructed.Range[0], _beingConstructed.Range[1] + 1), 2).PadLeft(_beingConstructed.ChromosomeLenght, '0');
                chrmosomes.Add(new Chromosome(gens));
            }

            _beingConstructed.Chromosomes = chrmosomes;
            return _beingConstructed;
        }
    }

    namespace PopulationBuiliderFluentApi
    {
        interface IPopSize
        {
            IAdaptationFunc PopulationSize(int popSize);
        }

        interface IAdaptationFunc
        {
            IRange FitnessFunction(IFunction function);
        }

        interface IRange
        {
            IGenPop Range(int rangeFrom, int rangeTo);
        }

        interface IGenPop
        {
            Population GeneratePopulation();
        }
    }
}
