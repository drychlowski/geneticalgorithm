﻿using System;
using System.Text;

namespace GeneticAlgorithm
{
    public static class QueuedConsole
    {
        private static readonly StringBuilder _sb = new StringBuilder();

        public static void AddToPrint(string message)
        {
            _sb.AppendLine(message);
        }

        public static void PrintAll()
        {
            Console.SetBufferSize(Console.BufferWidth, short.MaxValue - 1);
            Console.WriteLine(_sb.ToString());
            _sb.Clear();
        }
    }
}
