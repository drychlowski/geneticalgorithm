﻿using System;
using System.Linq;

namespace GeneticAlgorithm
{
    class Polynomial : IFunction
    {
        private readonly double[] _parameters;

        public Polynomial(params double[] parameters)
        {
            _parameters = parameters;
        }

        public double Value(double x)
        {
            return _parameters.Select((t, i) => t * Math.Pow(x, _parameters.Length - (i + 1))).Sum();
        }
    }
}
