﻿namespace GeneticAlgorithm
{
    interface IFunction
    {
        double Value(double x);
    }
}
